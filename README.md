# Сервис "Рассылка"



## Описание
Тестовое задание для комании "фабрика решений". Реализованы все обязательные пункты тз, а также многие из раздела "со звездочкой", такие как:
* организовать тестирование написанного кода
* подготовить docker-compose для запуска всех сервисов проекта одной командой
* сделать так, чтобы по адресу /docs/ открывалась страница со Swagger UI и в нём отображалось описание разработанного API.
* реализовать администраторский Web UI для управления рассылками и получения статистики по отправленным сообщениям
* удаленный сервис может быть недоступен, долго отвечать на запросы или выдавать некорректные ответы. Необходимо организовать обработку ошибок и откладывание запросов при неуспехе для последующей повторной отправки. Задержки в работе внешнего сервиса никак не должны оказывать влияние на работу сервиса рассылок.
* обеспечить подробное логирование на всех этапах обработки запросов

## Запуск проекта через Docker
* Скачиваем проект на свое устройство

* Создаем файл .env в корневой директории проекта(по подобию env.sample)
```
SECRET_KEY = 'django-insecure-&d(3l5kj^gb3p)(5$zfp3ausewf(d78$rom_e$2fz$)rd%h+de'
DEBUG = False
ALLOWED_HOSTS = '*'
DB_NAME = postgres
DB_PORT = 5432
DB_HOST = db
POSTGRES_USER = postgres
POSTGRES_PASSWORD = postgres
REDIS_HOST = localhost
REMOTE_API_TOKEN = eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE3MzQxODQ0NDEsImlzcyI6ImZhYnJpcXVlIiwibmFtZSI6Imh0dHBzOi8vdC5tZS9HcmluZGVsd2FsZG9mZiJ9.DevzmOly1SUrrDzaN15DjU8HrP7kAjAsG6f-WrKk2OQ
REMOTE_API_HOST = "https://probe.fbrq.cloud/v1/send/{pk}"
```

* переходим в папку infra
```
cd infra/
```

* Запускаем контейнеры
```
sudo docker-compose up --build -d
```

* Важно собрать статику у django и применить миграции
```
sudo docker exec -it infra-webapp-1 bash

python manage.py makemigrations
python manage.py migrate
python manage.py collectstatic --no-input
```

* Здесь же создаем суперпользователя
```
python manage.py createsuperuser
```

Админка будет находиться по адресу:
```
http://localhost/admin/login/?next=/admin/
```

Документация:
```
http://localhost/docs/
```


## Тесты
Чтобы запустить тесты проекта необходимо выполнить следующие шаги:
1. зайти в bash контейнера
```
sudo docker exec -it infra-webapp-1 bash
```
2. запустить тесты
```
pytest
```

## Разработчики
tg - @Grindelwaldoff