from django.core.validators import RegexValidator
from django.utils.translation import gettext_lazy as _
from django.conf import settings


telephone_validator = RegexValidator(
    regex=settings.TEL_REGEX,
    message=_(
        "Телефон должен быть передан в формате 7XXXXXXXXXX!"
    )
)
