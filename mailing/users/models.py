from django.db import models
from django.conf import settings

from users.validators import telephone_validator


class Client(models.Model):
    def tel_operator_init(self, tel_num):
        return int(str(tel_num)[1:4])

    tel_num = models.BigIntegerField(
        "Номер телефона",
        validators=[telephone_validator],
        unique=True
    )
    tel_operator = models.IntegerField(
        "Код оператора",
        blank=True,
        null=True,
        help_text=(
            "Это поле не обязательно указывать, оно само "
            "установит необходимое значение из введенного вами телефона"
        ),
    )
    tag = models.CharField(
        "Тэг",
        max_length=settings.MAX_CHARFIELD,
        blank=True,
        null=True,
    )
    timezone = models.CharField(
        "Часовой пояс",
        max_length=settings.TIMEZONE_LENGTH,
        choices=settings.TIMEZONES,
    )

    def save(self, *args, **kwargs) -> None:
        self.tel_operator = self.tel_operator_init(self.tel_num)
        return super().save(*args, **kwargs)
