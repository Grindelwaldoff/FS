import pytest
from datetime import datetime, timedelta


@pytest.fixture
def mailing():
    from mails.models import Mailing

    return Mailing.objects.create(
        message='Тестовая рассылка',
        filter='',
        start_time=datetime.now(),
        end_time=datetime.now() + timedelta(hours=1),
    )


@pytest.fixture
def test_client():
    from users.models import Client

    return Client.objects.create(
        tel_num=79812234243,
        timezone='UTC'
    )


@pytest.fixture
def client_2():
    from users.models import Client

    return Client.objects.create(
        tel_num=79812334243,
        timezone='UTC'
    )


@pytest.fixture
def client_3():
    from users.models import Client

    return Client.objects.create(
        tel_num=79812134243,
        timezone='UTC'
    )


@pytest.fixture
def message(mailing, test_client):
    from mails.models import Messages

    return Messages.objects.create(
        receiver=test_client,
        mailing=mailing,
        status='done'
    )


@pytest.fixture
def message_2(mailing, client_2):
    from mails.models import Messages

    return Messages.objects.create(
        receiver=client_2,
        mailing=mailing,
        status='await'
    )


@pytest.fixture
def message_3(mailing, client_3):
    from mails.models import Messages

    return Messages.objects.create(
        receiver=client_3,
        mailing=mailing,
        status='in_work'
    )
