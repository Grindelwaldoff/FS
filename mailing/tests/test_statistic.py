import pytest


class TestCase:
    endpoint = "/api/v1/report/"
    detail = "/api/v1/report/{pk}/"

    @pytest.mark.django_db(transaction=True)
    def test_statistic_get(self, client):
        response = client.get(self.endpoint)
        assert (
            response.status_code != 404
        ), f"Проверьте наличие {self.endpoint} в файле urls.py"
        assert response.status_code != 400, (
            f"Проверьте, что при запросе к {self.endpoint}"
            " неавторизованным пользователем не возвращается код 400"
        )

    @pytest.mark.django_db(transaction=True)
    def test_statistic_response(
        self, client, mailing, message, message_2, message_3
    ):
        response = client.get(self.detail.format(pk=mailing.id))
        assert response.status_code == 200, (
            f"Првоерьте что при запросе к эндпоинту {self.detail}"
            " возвращается код 200"
        )
        json_data = response.json()
        print(json_data)
        assert (
            json_data["report"]["await"] == 1
        ), "Проверьте корректность подсчета сообщений рассылки в статусе await"
        assert (
            json_data["report"]["in_work"] == 1
        ), "Проверьте корректность подсчета сообщений рассылки в статусе in_work"
        assert (
            json_data["report"]["done"] == 1
        ), "Проверьте корректность подсчета сообщений рассылки в статусе done"
