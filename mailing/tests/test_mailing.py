import pytest
from datetime import datetime, timedelta
from django.conf import settings

from mails.models import Mailing


class TestCase:
    endpoint = "/api/v1/mailing/"
    detail = "/api/v1/mailing/{pk}/"
    data = {
        "start": datetime.now().strftime(settings.DATETIME_FORMAT),
        "message": "Тестовая расслыка",
        "end": (datetime.now() + timedelta(hours=1)).strftime(
            settings.DATETIME_FORMAT
        ),
        'filter': '981'
    }

    @pytest.mark.django_db(transaction=True)
    def test_mailing_access(self, client):
        response = client.get(self.endpoint)
        assert (
            response.status_code != 404
        ), f"проверьте наличие эндпоинта {self.endpoint} в файле urls.py"
        assert (
            response.status_code != 400
        ), f"Првоерьет доступ к {self.endpoint}"

    @pytest.mark.django_db(transaction=True)
    def test_mailing_create(self, client):
        amount_inst = Mailing.objects.count()
        print(self.data)
        response = client.post(self.endpoint, data=self.data)
        assert response.status_code == 201, (
            f"Проверьте что при POST запрсое к {self.endpoint} "
            "c валидными данными возвращается код 201"
        )
        assert (
            Mailing.objects.count() == amount_inst + 1
        ), "проверьте что олько что созданная рассылка появляется в бд"
        self.data.update(
            {
                "end": (datetime.now() - timedelta(hours=5)).strftime(
                    settings.DATETIME_FORMAT
                )
            }
        )
        response = client.post(self.endpoint, data=self.data)
        assert response.status_code != 201, (
            f"Проверьте, что при POST запросе к {self.endpoint}"
            " с не валидными данными не возвращается код 201"
        )

    @pytest.mark.django_db(transaction=True)
    def test_update_mailing(self, client, mailing):
        new_date = datetime.now() + timedelta(minutes=15)
        data = {"start": new_date.strftime(settings.DATETIME_FORMAT)}
        response = client.patch(
            self.detail.format(pk=mailing.id),
            data=data,
            content_type="application/json",
        )
        assert response.status_code == 200, (
            f"Првоерьте что при PUT запрсое к {self.detail} "
            "с валидными данными возвращается код 200"
        )
        assert datetime.strptime(
            response.json()["start"], "%Y-%m-%dT%H:%M:%SZ"
        ).strftime(settings.DATETIME_FORMAT) == new_date.strftime(
            settings.DATETIME_FORMAT
        ), (
            "Проверьте, что при изменениях объекта рассылки "
            "данные действительно меняются"
        )
        data.update({"end": datetime.now()})
        response = client.put(self.detail.format(pk=mailing.id), data=data)
        assert response.status_code != 200, (
            f"Првоерьте что при PUT запрсое к {self.detail} "
            "с не валидными данными не возвращается код 200"
        )

    @pytest.mark.django_db(transaction=True)
    def test_mailing_delete(self, client, mailing):
        amuont_inst = Mailing.objects.count()
        response = client.delete(self.detail.format(pk=mailing.id))
        assert response.status_code == 204, (
            f"Проверьте что при DELETE запросе к {self.detail} "
            "возвращается код 204"
        )
        assert Mailing.objects.count() == amuont_inst - 1, (
            "Проверьть что рассылка после DELETE зарпоса " "удаляется из бд"
        )
