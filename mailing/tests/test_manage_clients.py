import pytest

from users.models import Client


class TestCase:
    endpoint = "/api/v1/manage-users/"
    detail = "/api/v1/manage-users/{pk}/"
    data = {
        "tel_num": 79238238425,
        "tag": "test_user",
        "timezone": "UTC",
    }

    @pytest.mark.django_db(transaction=True)
    def test_users_access(self, client):
        response = client.get(self.endpoint)
        assert response.status_code != 404, (
            f"Првоерьте наличие эндпоинта {self.endpoint} " "в файле urls.py"
        )

    @pytest.mark.django_db(transaction=True)
    def test_user_create(self, client):
        amount_inst = Client.objects.count()
        response = client.post(self.endpoint, data=self.data)
        print(self.data)
        assert response.status_code == 201, (
            f"Проверьте, что при POST запросе к {self.endpoint} "
            "с валидными даными возвращается код 201"
        )
        assert Client.objects.count() == amount_inst + 1, (
            "Проверьте, что при сущность клиента появилась в бд "
            "после post запроса"
        )

    @pytest.mark.django_db(transaction=True)
    def test_update_client_data(self, client, test_client):
        response = client.patch(
            self.detail.format(pk=test_client.id),
            data={"tel_num": 71111111111},
            content_type="application/json",
        )
        assert response.status_code == 200, (
            f"Првоерьте что при PATCH запросе к {self.detail} "
            "с валидными данными возвращается код 200"
        )
        client = Client.objects.filter(id=test_client.id).first()
        assert client.tel_num == 71111111111, (
            "Проверьте что после PATCH запроса "
            "данные клиента действительно обновляются"
        )

    @pytest.mark.django_db(transaction=True)
    def test_user_delete(self, client, test_client):
        amount_inst = Client.objects.count()
        response = client.delete(self.detail.format(pk=test_client.id))
        assert response.status_code == 204, (
            f"Проверьте что при DELETE запросе к {self.detail} "
            "возвращается код 204"
        )
        assert Client.objects.count() == amount_inst - 1, (
            "Проверьте что после запроса на удаление клиента из базы "
            "его объект действительно пропадает"
        )
