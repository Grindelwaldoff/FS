from django.contrib import admin

from mails.models import Mailing, Messages


admin.site.register(Messages)
admin.site.register(Mailing)
