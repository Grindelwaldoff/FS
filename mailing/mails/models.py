from datetime import datetime

from django.db import models
from django.conf import settings

from users.models import Client


class Mailing(models.Model):
    start_time = models.DateTimeField(
        "Время начала рассылки", default=datetime.now
    )
    message = models.TextField(
        "Текст сообщения клиенту",
        max_length=400,
    )
    filter = models.CharField(
        "Параметр фильтрации",
        max_length=settings.MAX_CHARFIELD,
    )
    end_time = models.DateTimeField(
        "Время окончания рассылки",
    )

    def __str__(self) -> str:
        return self.message

    class Meta:
        verbose_name = "Рассылка"
        verbose_name_plural = "Рассылки"


class Messages(models.Model):
    STATUS_CHOICES = (
        ("await", "Ожидает исполнения"),
        ("in_work", "Исполняется"),
        ("done", "Выполнено"),
    )

    created = models.DateTimeField("Дата создания", auto_now_add=True)
    status = models.CharField(
        "Статус",
        choices=STATUS_CHOICES,
        blank=True,
        null=True,
        max_length=settings.MAX_CHARFIELD,
    )
    receiver = models.ForeignKey(
        Client,
        related_name="messages",
        on_delete=models.CASCADE,
        verbose_name="Получатель",
    )
    mailing = models.ForeignKey(
        Mailing,
        related_name="messages",
        on_delete=models.CASCADE,
        verbose_name="Объект рассылки",
    )

    def save(self, *args, **kwargs):
        if not self.status:
            self.status = 'await'
        super().save(*args, **kwargs)

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=['receiver', 'mailing'],
                name='unique_receiver_mailing_fields'
            ),
        ]
        verbose_name = "Сообщение"
        verbose_name_plural = "Сообщения"
