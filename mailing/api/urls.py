from django.urls import path, include
from rest_framework.routers import DefaultRouter

from api.views import MailingViewSet, ClientViewSet, ReportViewSet


router = DefaultRouter()
router.register('mailing', MailingViewSet)
router.register('manage-users', ClientViewSet)
router.register('report', ReportViewSet)


urlpatterns = [
    path('', include(router.urls)),
]
