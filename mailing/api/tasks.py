import requests
import logging
from retrying import retry
from requests.exceptions import (
    HTTPError,
    Timeout,
    ConnectionError,
)
from time import sleep
from datetime import datetime, timedelta

from celery import shared_task
from django.conf import settings

from mails.models import Messages, Mailing


log = logging.getLogger("faker")


def retry_exception(exception):
    return any(
        [
            isinstance(exception, HTTPError),
            isinstance(exception, Timeout),
            isinstance(exception, ConnectionError),
        ]
    )


@retry(
    retry_on_exception=retry_exception,
    stop_max_attempt_number=5,
    wait_exponential_multiplier=2000,
    wait_exponential_max=16000,
)
def safe_request(request_data):
    return requests.post(
        url=settings.FOREIGN_API_URL.format(pk=request_data['id']),
        json=request_data,
        headers={"Authorization": "Bearer " + settings.FOREIGN_API_TOKEN},
    )


def send_message_to_api(id, message, tel_num, retries=0):
    # формирование запроса к API
    request_data = {
        "id": id,
        "text": message,
        "phone": tel_num,
    }
    log.info(
        f"Запрос к стороннему апи {settings.FOREIGN_API_URL.format(pk=id)}"
    )
    # обработка всех наиболее вероятных исключений при запросе
    response = safe_request(request_data)
    log.info(f"Результат запроса {response.json()}")
    log.info(f"Статус код запроса {response.status_code}")
    return response.status_code


def set_pause(start, mailing):
    seconds = (
        abs(
            datetime.now()
            - datetime.strptime(start, settings.ORIGINAL_DATETIME_FORMAT)
        )
        / timedelta(microseconds=1)
        / 1000000
    )
    log.info(f"Начало сна на {seconds}")
    sleep(seconds)
    log.info(f"Начало обработки сообщений для рассылки {mailing}")


@shared_task()
def mailing_execution(response):
    # подсчет времени ожидания до исполнения рассылки
    set_pause(response["start"], response["id"])
    mailing_inst = Mailing.objects.get(id=response["id"])
    for message in Messages.objects.filter(
        mailing_id=mailing_inst.id, status="await"
    ):
        if datetime.now() < datetime.strptime(
            response["end"], settings.ORIGINAL_DATETIME_FORMAT
        ):
            log.info(
                f"Обработка отправки сообщений рассылки {mailing_inst.id}"
            )
            # отправка сообщений в указанное время
            message.status = "in_work"
            message.save()
            try:
                code = send_message_to_api(
                    message.id,
                    message.mailing.message,
                    message.receiver.tel_num,
                )
                if code == 200:
                    message.status = "done"
                    message.save()
                    log.info(
                        "Сообщений пользователю "
                        f"{message.receiver.tel_num} отправлено"
                    )
            except Exception as e:
                log.error(e)
        else:
            log.info(
                "Время отведенное на рассылку закончилось, "
                "больше сообщения по рассылке {mailing_inst} не отправляются"
            )
