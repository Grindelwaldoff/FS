from datetime import timedelta

from rest_framework import serializers
from drf_spectacular.utils import (
    extend_schema_serializer,
    OpenApiExample,
)
from django.utils import timezone
from django.conf import settings

from mails.models import Mailing, Messages
from users.models import Client


@extend_schema_serializer(
    examples=[
        OpenApiExample(
            "Valid example 1",
            value=[
                {
                    "start": "16.12.2023 4:20",
                    "message": "string",
                    "filter": "string",
                    "end": "16.12.2023 4:26"
                }
            ],
            request_only=True
        ),
    ],
)
class MailingSerializer(serializers.ModelSerializer):
    start = serializers.DateTimeField(
        input_formats=(settings.DATETIME_FORMAT,),
        source="start_time",
        required=False,
    )
    end = serializers.DateTimeField(
        input_formats=(settings.DATETIME_FORMAT,),
        source="end_time",
        required=False,
    )

    class Meta:
        model = Mailing
        fields = ("id", "start", "message", "filter", "end")
        read_only_fields = ("id",)

    def create(self, validated_data):
        if (
            validated_data["start_time"] + timedelta(minutes=5)
            >= validated_data["end_time"]
        ) and (
            (validated_data["start_time"] + timedelta(minutes=1))
            > timezone.now()
        ):
            raise serializers.ValidationError(
                "Время начала рассылки должно быть меньше даты окончания,"
                "и отличаться хотя бы на 5 мин. Также минимальное валидное "
                "значение для поля start - это нынешнее значение дня, "
                "месяца, часа, года..."
            )
        return super().create(validated_data)


@extend_schema_serializer(
    examples=[
        OpenApiExample(
            "Valid example 1",
            value=[
                {
                    "tel_num": 79991234567,
                    "tag": "string",
                    "timezone": "Africa/Abidjan"
                }
            ],
        ),
    ],
)
class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = (
            "id",
            "tel_num",
            "tag",
            "tel_operator",
            "timezone",
        )
        read_only_fields = ("id", 'tel_operator')


@extend_schema_serializer(
    examples=[
        OpenApiExample(
            "Valid example 1",
            value=[
                {
                    "mailing_id": {
                        "await": 3,
                        "in_work": 7,
                        "done": 9,
                    },
                },
            ],
        ),
    ],
)
class ReportSerializer(serializers.ModelSerializer):
    mailing_id = serializers.IntegerField(source="id")
    report = serializers.SerializerMethodField()

    class Meta:
        model = Mailing
        fields = ("mailing_id", "report")

    def get_report(self, obj):
        return {
            "await": Messages.objects.filter(
                mailing_id=obj.id, status="await"
            ).count(),
            "in_work": Messages.objects.filter(
                mailing_id=obj.id, status="in_work"
            ).count(),
            "done": Messages.objects.filter(
                mailing_id=obj.id, status="done"
            ).count(),
        }
