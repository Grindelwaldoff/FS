import logging

from rest_framework.viewsets import ModelViewSet, GenericViewSet
from rest_framework.mixins import ListModelMixin, RetrieveModelMixin

from api.serializers import (
    MailingSerializer,
    ClientSerializer,
    ReportSerializer,
)
from mails.models import Mailing, Messages
from users.models import Client
from api.tasks import mailing_execution


logger = logging.getLogger("faker")


class MailingViewSet(ModelViewSet):
    queryset = Mailing.objects.all()
    serializer_class = MailingSerializer
    http_method_names = ["post", "delete", "patch"]

    def create(self, request, *args, **kwargs):
        response = super().create(request, *args, **kwargs)
        if "id" in response.data:
            mailing_inst = Mailing.objects.filter(
                id=response.data.get("id")
            ).first()
            clients = []
            if response.data.get("filter").isdigit():
                # если в фильтр передан мобильный код региона
                filter_param = int(response.data.get("filter"))
                if len(str(filter_param)) != 3:
                    logger.error(
                        "Мобильный код региона, указанный в фильтрах,"
                        " должен быть длинной 3 символа!"
                    )
                clients = Client.objects.filter(tel_operator=filter_param)
            else:
                # если в фильтр передано значения тэга
                filter_param = response.data.get("filter")
                clients = Client.objects.filter(tag=filter_param)
            # создание сообщений для статистики в дальнейшем
            if len(clients) > 0:
                for client in clients:
                    Messages.objects.create(
                        receiver=client, mailing=mailing_inst, status="await"
                    )
                mailing_execution.apply_async(args=[response.data])
        return response


class ReportViewSet(ListModelMixin, RetrieveModelMixin, GenericViewSet):
    queryset = Mailing.objects.all()
    serializer_class = ReportSerializer


class ClientViewSet(ModelViewSet):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer
    http_method_names = ["post", "delete", "patch"]
